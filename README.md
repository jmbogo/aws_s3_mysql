Instructions.
=============================
Note: You should have the following already in place
 -S3 bucket created
 -aws_key and secret
 -aws simple notification service topic and subscription created
 -Hosts  file configured with the RDS Mysql instance
 -A control machine with awscli,Ansible,python and pip installed
 
 1: Start bash command line tool
 2: connect to aws using key and secret
 3: Save the playbook under /etc/ansible/roles
 4: Run below command from bash prompt
		$ ansible-playbook -s mysqlbackup.yml
		